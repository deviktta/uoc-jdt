<?php

require_once __DIR__ . '/src/Service/DatabaseService.php';
require_once __DIR__ . '/src/Service/HtmlService.php';
require_once __DIR__ . '/src/Service/SystemService.php';

$current_lang = "ca";
$title = "Inicialització de la base de dades";

$database = new DatabaseService();
$username = "admin";
$password = md5("3141592");
$post = $_POST; 
if (!empty($post['username']) && !empty($post['password'])) {
  SystemService::setAdmin($post['username'] === $username && md5($post['password']) === $password);
}
?>
<html>
  <?php echo HtmlService::getHead('ca', $title); ?>
  <body>
    <?php echo HtmlService::renderHeader($current_lang); ?>

    <main id="site-content" class="container">
      <div class="row">
        <h1 class="col-12 text-center"><?php echo $title ?></h1>
      </div>

      <?php if (SystemService::isAdmin()) { ?>
      <div class="row">      
        <h2 class="col-12">Tasca 1: creació de la base de dades i d'un usuari per operar amb ella</h2>
        <div class="col-12">
          <p>Enregistrament de la tasca:</p>
          <div class="log">
            <?php $database->initDatabase(); ?>
          </div>
        </div>
      </div>

      <div class="row">      
        <h2 class="col-12">Tasca 2: creació de les taules</h2>
        <div class="col-12">
          <p>Enregistrament de la tasca:</p>
          <div class="log">
            <?php $database->initTables(); ?>
          </div>
        </div>
      </div>

      <?php } else { ?>

      <div class="row">      
        <div class="col-12">
          <h2>Inicia sessió com administrador per accedir-hi:</h2>
          <form method="POST">
            <input type="text" placeholder="Usuari" name="username" style="display: block;" required="required" />
            <input type="password" placeholder="Mot de pas" name="password" style="display: block;" required="required" />
            <input type="submit" value="Iniciar sessió" />
          </form>
        </div>
      </div>

      <?php } ?>
    </main>
    
  </body>
</html>