<?php

require_once __DIR__ . '/../src/Entity/Category.php';
require_once __DIR__ . '/../src/Service/HtmlService.php';
require_once __DIR__ . '/../src/Service/SystemService.php';

$current_lang = 'ca';
$title = "Comandes";

$username = "admin";
$password = md5("3141592");
$post = $_POST; 
if (!empty($post['username']) && !empty($post['password'])) {
  SystemService::setAdmin($post['username'] === $username && md5($post['password']) === $password);
}
?>
<html>
  <?php echo HtmlService::getHead($current_lang, $title); ?>

  <body class="page-type-cart">
    <?php echo HtmlService::renderHeader($current_lang); ?>

    <main id="site-content" class="container">
      <div class="row">
        <h1 class="col-12"><?php echo $title ?></h1>
          <div class="col-12" style="display: flex; justify-content: center; flex-direction: column;">
          <?php if (SystemService::isAdmin()) {

            $databaseService = new DatabaseService();
            $query = "SELECT * FROM OrderData";
            $orders = $databaseService->query($query, FALSE);
            while ($oder = $orders->fetch_assoc()) {

              $timestamp = $oder['timestamp'];
              $full_name = $oder['full_name'];
              $province = $oder['province'];
              $address = $oder['address'];
              $email = $oder['email'];
              $phone = $oder['phone'];
              $total_price = $oder['price_total'] / 100;
            ?>

            <div class="order" style="border: 1px solid #000000; padding: 20px; margin-bottom: 20px;">
              <h2>Comanda <?php echo $oder['id'] . ", " . $timestamp ?>:</h2>
              <h5><u>Dades del client</u></h5>
              <ul style="padding: 0px; margin-left: 17px;">
                <li>Nom complet: <?php echo $full_name ?></li>
                <li>Provincia: <?php echo $province ?></li>
                <li>Adreça: <?php echo $address ?></li>
                <li>Adreça de correu electrònic: <?php echo $email ?></li>
                <li>Telèfon de contacte: <?php echo $phone ?></li>
                <li><b>Preu total: <?php echo number_format($total_price, 2, ",", "."); ?> €</b></li>
              </ul>
              <h5 style="margin-top: 20px;"><u>Articles venuts</u></h5>
              <ul style="padding: 0px; margin-left: 17px;">
            <?php

              $query = "SELECT * FROM OrderLine WHERE order_id=" . $oder['id'] . ";";
              $order_lines = $databaseService->query($query, FALSE);
              while ($order_line = $order_lines->fetch_assoc()) {

                $game_id = $order_line['id'];
                $query = "SELECT Name FROM Game WHERE id=" . $game_id . ";";
                $game = $databaseService->query($query, FALSE)->fetch_assoc();
                $game_name = $game['Name'];
                $quantity = $order_line['quantity'];
                $price_total = $order_line['price_total'];
                $price_total_str = number_format($price_total / 100, 2, ",", ".");
            ?>

                <li><?php echo $quantity ?> x <?php echo $game_name ?>: <?php echo $price_total_str ?> €</li>

            <?php              
              }
              ?>
                
              </ul>
            </div>
            <?php }
          ?>
          <?php } else { ?>
            <h2>Iniciar sessió com administrador per accedir a les comandes:</h2>
            <form method="POST">
              <input type="text" placeholder="Usuari" name="username" style="display: block;" required="required" />
              <input type="password" placeholder="Mot de pas" name="password" style="display: block;" required="required" />
              <input type="submit" value="Iniciar sessió" />
            </form>
          <?php } ?>
          </div>
      </div>
    </main>

  </body>
</html>