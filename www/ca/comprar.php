<?php

require_once __DIR__ . '/../src/Entity/Category.php';
require_once __DIR__ . '/../src/Service/HtmlService.php';
require_once __DIR__ . '/../src/Service/SystemService.php';
require_once __DIR__ . '/../src/Service/CartService.php';

$current_lang = 'ca';
$title = "Compra";

$post = $_POST;
if (!empty($post['id']) && !empty($post['type'])) {
  // S'ha afegit un product a la cistella.
  CartService::update($post['id'], ($post['type'] === 'add') ? 1 : -1);
}

$cart = new CartService();
$order = $cart->getOrderLines();
//SystemService::debug($cart->getOrderPrice());
?>
<html>
  <?php echo HtmlService::getHead($current_lang, $title); ?>

  <body class="page-type-cart">
    <?php echo HtmlService::renderHeader($current_lang); ?>

    <main id="site-content" class="container">
      <div class="row">
        <h1 class="col-12"><?php echo $title ?></h1>
      </div>
      <?php if (!empty($order)) { ?>
        <div class="row">
        <div class="col-8">
          <h3>Resum de la cistella:</h3>
          <table id="cart" style="width: 100%">
            <thead>
              <tr>
                <th>NOM DE L'ARTICLE</th>
                <th>PREU TOTAL</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($order as $order_line) { ?>
                <tr>
                  <td><?php echo $order_line['quantity'] . " x " . $order_line['name']; ?></td>
                  <td><?php echo $order_line['price_total_str']; ?> €</td>
                </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>
        <div class="col-4">
          <h3>Dades personals:</h3>
          <small><i>Tots els camps són obligatoris</i></small>
          <form class="user" action="/ca/gracies.php" method="POST">
            <input type="text" name="full_name" placeholder="Nom complet" required="required" />
            <input type="text" name="province" placeholder="Provincia" required="required" />
            <input type="text" name="address" placeholder="Adreça" required="required" />
            <input type="text" name="email" placeholder="Adreça de correu electrònic" required="required" />
            <input type="text" name="phone" placeholder="Telèfon de contacte" required="required" />
            <input type="submit" value="Pagar <?php echo $cart->printOrderPrice() ?> €" />
          </form>
        </div>
      </div>
      <?php } else { ?>
        <div class="row">
          <div class="col-12">
            <p>Abans d'accedir a la pàgina de compra has d'afegir productes a la cistella.</p>
          </div>
        </div>
      <?php } ?>

    </main>

  </body>
</html>