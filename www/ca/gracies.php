<?php

require_once __DIR__ . '/../src/Entity/Category.php';
require_once __DIR__ . '/../src/Service/HtmlService.php';
require_once __DIR__ . '/../src/Service/SystemService.php';
require_once __DIR__ . '/../src/Service/CartService.php';

$current_lang = 'ca';
$title = "Gràcies per la seva compra";
  
$cart = new CartService();
$order = $cart->getOrderLines();
$page_status = FALSE;

$post = $_POST;
//SystemService::debug($post['price_total']); 
if (!empty($post['full_name']) && !empty($post['province']) && !empty($post['address']) && !empty($post['email']) && !empty($post['phone']) && !empty($order)) {
  $page_status = TRUE;
  // Desar la comanda amb les dades del usuari - BEGIN
  $user = $post;
  $timestamp = date('Y-m-d H:i:s',time());
  $full_name = $post['full_name'];
  $province = $post['province'];
  $address = $post['address'];
  $email = $post['email'];
  $price_total = $cart->getOrderPrice() * 100;
  $phone = $post['phone'];

  $databaseService = new DatabaseService();
  $query = "INSERT INTO OrderData (timestamp, full_name, province, address, email, price_total, phone) VALUES ("
    . "\"" . $timestamp . "\","
    . "\"" . $full_name . "\","
    . "\"" . $province . "\","
    . "\"" . $address . "\","
    . "\"" . $email . "\","
    . "\"" . $price_total . "\","
    . "\"" . $phone . "\")";
  $databaseService->query($query, FALSE);
  $query = "SELECT LAST_INSERT_ID();";
  $result = $databaseService->query($query, FALSE)->fetch_assoc();
  $order_db_id = $result["LAST_INSERT_ID()"];
  // Desar la comanda amb les dades del usuari - END

  foreach ($order as $order_line) {
    $game = $order_line['id'];
    $quantity = $order_line['quantity'];
    $price_total = $order_line['price_total'] * 100;
    $query = "INSERT INTO OrderLine (game, quantity, price_total, order_id) VALUES ("
      . "\"" . $game . "\","
      . "\"" . $quantity . "\","
      . "\"" . $price_total . "\","
      . "\"" . $order_db_id . "\")";
    $databaseService->query($query, FALSE);
  }
  
  $cart->destroy();
}
?>
<html>
  <?php echo HtmlService::getHead($current_lang, $title); ?>

  <body class="page-type-cart">
    <?php echo HtmlService::renderHeader($current_lang); ?>

    <main id="site-content" class="container">
      <div class="row">
        <h1 class="col-12"><?php echo ($page_status) ? $title : "Cistella buida" ?></h1>
      </div>
    </main>

  </body>
</html>