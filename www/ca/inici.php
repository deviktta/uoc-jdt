<?php

require_once __DIR__ . '/../src/Entity/Category.php';
require_once __DIR__ . '/../src/Service/HtmlService.php';
require_once __DIR__ . '/../src/Service/SystemService.php';

$current_lang = 'ca';
$title = "Pàgina d'inici";

$get = $_GET;
if (!empty($get['action']) && $get['action'] === 'logout') {
  SystemService::setAdmin(FALSE);
}
?>
<html>
  <?php echo HtmlService::getHead($current_lang, $title); ?>

  <body class="page-type-home">
    <?php echo HtmlService::renderHeader($current_lang); ?>

    <main id="site-content" class="container">
      <div class="row">
        <h1 class="col-12"><?php echo $title ?></h1>
      </div>
      <div class="row featured-list">
        <?php echo Category::renderHomeList($current_lang); ?>
      </div>
    </main>

  </body>
</html>