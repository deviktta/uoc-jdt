<?php

require_once __DIR__ . '/Entity.php';
require_once __DIR__ . '/Category.php';
require_once __DIR__ . '/../Service/SystemService.php';

/**
 * Clase per gestionar els jocs.
 */
class Game extends Entity {
  
  /**
   * Segment URL al qual pertany.
   */
  const ENTITY_URI = "/jocs";
  
  /**
   * Nom de la taula de la base de dades.
   */
  const DB_TABLE = "Game";
  
  /**
   * Nom.
   *
   * @var string
   */
  protected $name;
  
  /**
   * Descripció.
   *
   * @var string
   */
  protected $description;
  
  /**
   * Preu.
   *
   * @var float
   */
  protected $price;
  
  /**
   * Enllaç relatiu (ha de començar amb '/').
   *
   * @var string
   */
  protected $url;
  
  /**
   * Categories.
   *
   * @var array 
   */
  protected $category;
  
  /**
   * Bloc HTML que mostra les categories en forma d'enllaços.
   *
   * @param string $lang
   *   Idioma del contingut.
   *
   * @param string $category_id
   *   Identificador de la categoria.
   *
   * @return string
   *   Codi HTML.
   */
  public static function renderGameListByCategoryId(string $lang, string $category_id) {
    switch ($lang) {

      case 'ca':
        $games = self::getAll();
        foreach ($games as $id => $data) {
          if ($data['category'] !== $category_id) {
            unset($games[$id]);
          }
        }
        $category = new Category($category_id);
        
        $html = ""
          . "<h2 class=\"col-12\">Llistat de jocs:</h2>"
          . "<div class=\"col-12 item-list\">";
        foreach ($games as $game) {
          $name = $game['name'];
          $description = $game['description'];
          $url = "/ca" . self::ENTITY_URI . $game['url'];
          $html .= ""
              . "<a href=\"$url\" class=\"item-content\">"
                . "<img src=\"/assets/images/joc.jpg\" />"
                . "<h3 class=\"item-name\">$name</h3>"
                . "<p class=\"item-description\">$description</p>"
              . "</a>";
        }
        $html .= "</div>";
        break;

      default:
        die(__CLASS__ . "::" . __METHOD__ . " - Language not found");
        break;
    
    }

    return $html;
  }
  
  /**
   * Bloc HTML que mostra les categories en forma d'enllaços.
   *
   * @param string $lang
   *   Idioma del contingut.
   *
   * @param string $category_id
   *   Identificador de la categoria.
   *
   * @return string
   *   Codi HTML.
   */
  public static function renderAddGameForm(string $lang, string $game_id) {    
    switch ($lang) {

      case 'ca':
        $html = ""
      . "<form method=\"POST\">"
      . "<input type=\"hidden\" name=\"id\" value=\"$game_id\"/>"
      . "<input type=\"number\" name=\"quantity\" min=\"1\" max=\"9\" size=\"1\" step=\"1\" value=\"1\"/>"
      . "<input type=\"submit\" value=\"AFEGIR\" class=\"btn btn-info\" style=\"margin-left: 20px;\"/>"
      . "</form>";
        break;

      default:
        die(__CLASS__ . "::" . __METHOD__ . " - Language not found");
        break;
    
    }

    return $html;
  }

}
