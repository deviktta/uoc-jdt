<?php

require_once __DIR__ . '/Entity.php';

/**
 * Clase per gestionar les categories.
 */
class Category extends Entity {
  
  /**
   * Segment URL al qual pertany.
   */
  const ENTITY_URI = "/categories";
  
  /**
   * Nom de la taula de la base de dades.
   */
  const DB_TABLE = "GameCategory";
  
  /**
   * Nom.
   *
   * @var string
   */
  protected $name;
  
  /**
   * Descripció.
   *
   * @var string
   */
  protected $description;
  
  /**
   * Enllaç relatiu (ha de començar amb '/').
   *
   * @var string
   */
  protected $url;
  
  /**
   * Bloc HTML que mostra les categories en forma d'enllaços.
   *
   * @param string $lang
   *   Idioma del contingut.
   *
   * @return string
   *   Codi HTML.
   */
  public static function renderHomeList(string $lang) {
    switch ($lang) {

      case 'ca':
        $categories = self::getAll();

        $html = ""
          . "<h2 class=\"col-12\">Llistat de categories:</h2>"
          . "<div class=\"col-12 item-list\">";
        foreach ($categories as $category) {
          $name = $category['name'];
          $description = $category['description'];
          $url = "/ca" . self::ENTITY_URI . $category['url'];
          $img = "/assets/images/" . $category['image'];
          $html .= ""
              . "<a href=\"$url\" class=\"item-content\">"
                . "<img src=\"$img\" />"
                . "<h3 class=\"item-name\">$name</h3>"
                . "<p class=\"item-description\">$description</p>"
              . "</a>";
        }
        $html .= "</div>";
        break;

      default:
        die(__CLASS__ . "::" . __METHOD__ . " - Language not found");
        break;
    
    }

    return $html;
  }
}
