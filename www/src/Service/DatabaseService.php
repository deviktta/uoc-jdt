<?php

/**
 * Servei que dóna support a les conexiones a base de dades.
 */
class DatabaseService
{

  const CREDENTIALS = [
    'host' => '172.20.1.11',
    'port' => 3306,
    'name' => 'jdt',
    'user' => 'jdt',
    'pswd' => '2rX#l6q$z1dE5zHo',
  ];
  
  protected $isLogger = FALSE;

  private $database;   // MySQL connection

  public function __construct() {
    $this->setConnection();
    $db_name = self::CREDENTIALS['name'];
    $this->query("USE $db_name;", FALSE);
  }
  
  public function initDatabase()
  {
    $this->logger = TRUE;
    // Connect the database as 'root' for creating database and user
    $this->setConnection([
      'host' => self::CREDENTIALS['host'],
      'port' => self::CREDENTIALS['port'],
      'user' => 'root',
      'pswd' => 'root',
      'name' => '',
    ]);

    // Drop old database
    $this->logging("1. Dropping old database...");
    $query = "DROP DATABASE " . self::CREDENTIALS['name'];
    $this->query($query);

    // Create database
    $this->logging("2. Creating database...");
    $query = "CREATE DATABASE " . self::CREDENTIALS['name'];
    $this->query($query);

    // Create database user
    $this->logging("3. Creating database user...");
    $query = "CREATE USER IF NOT EXISTS '" . self::CREDENTIALS['user'] . "'@'%' IDENTIFIED BY '" . self::CREDENTIALS['pswd'] . "'";
    $this->query($query);

    // Give user full access to database
    $this->logging("4. Giving database user full access to out database...");
    $query = "GRANT ALL PRIVILEGES ON `jdt`.* TO '" . self::CREDENTIALS['user'] . "'@'%';";
    $this->query($query);

    // Finish
    $this->closeConnection();
  }
  
  public function initTables()
  {
    $this->logger = TRUE;
    // Connect the database as 'jdt' for creating database and user
    $this->setConnection();

    // GameCategory table
    $this->logging("1.1 Dropping old 'GameCategory' table...");
    $query = "DROP TABLE IF EXISTS GameCategory";
    $this->query($query);
    
    $this->logging("1.2 Creating new 'GameCategory' table...");
    $query = "CREATE TABLE GameCategory ("
      . "id INT UNSIGNED AUTO_INCREMENT,"
      . "name VARCHAR(255) NOT NULL,"
      . "description VARCHAR(255) NOT NULL,"
      . "url VARCHAR(255) NOT NULL,"
      . "image VARCHAR(255) NOT NULL,"
      . "PRIMARY KEY(id)"
      . ") ENGINE=INNODB CHARACTER SET utf8 COLLATE utf8_general_ci";
    $this->query($query);
    
    $this->logging("1.3 Inserting data for 'GameCategory' table...<br>");
    $query = "INSERT INTO GameCategory (name, description, url, image) VALUES (\"Un contra un\", \"Jocs especialment dissenyats per a dos jugadors: amb un amic, la teva parella... t'endinseràs en el frènetic món dels duels.\", \"/un-contra-un.php\", \"categoria-1vs1.jpg\")";
    $this->query($query);
    $query = "INSERT INTO GameCategory (name, description, url, image) VALUES (\"Per als més menuts\", \"Educatius i divertits, aquests jocs estàn dissenyats per els més petits, amb el que podràn desenvolupar les seves habilitats i destreses.\", \"/menuts.php\", \"categoria-menuts.jpg\")";
    $this->query($query);
    $query = "INSERT INTO GameCategory (name, description, url, image) VALUES (\"Experts\", \"Apte només per a jugadors amb recorregut en el món dels jocs de taula, podràs trobar-los d'estratègia, de recursos, de guerra...\", \"/experts.php\", \"categoria-experts.jpg\")";
    $this->query($query);

    // Game table
    $this->logging("2.1 Dropping old 'Game' table...");
    $query = "DROP TABLE IF EXISTS Game;";
    $this->query($query);
    
    $this->logging("2.2 Creating new 'Game' table...");
    $query = "CREATE TABLE Game ("
      . "id INT UNSIGNED AUTO_INCREMENT,"
      . "name VARCHAR(255) NOT NULL,"
      . "description VARCHAR(1020) NOT NULL,"
      . "price SMALLINT UNSIGNED NOT NULL,"
      . "category INT UNSIGNED,"
      . "url VARCHAR(255) NOT NULL,"
      . "PRIMARY KEY(id),"
      . "FOREIGN KEY(category) REFERENCES GameCategory(id)"
      . ") ENGINE=INNODB CHARACTER SET utf8 COLLATE utf8_general_ci";
    $this->query($query);
    
    $this->logging("2.3 Inserting data for 'Game' table...<br>");
    $query = "INSERT INTO Game (name, description, price, category, url) VALUES (\"7 Wonders Duel\", \"Fes prosperar la teva civilització, reuneix materials, construeix piràmades i aplasta el teu contricant.\", 1999, 1, \"/7-wonders-duel.php\")";
    $this->query($query);
    $query = "INSERT INTO Game (name, description, price, category, url) VALUES (\"Twilight Struggle\", \"En plena Guerra Freda, un jugador encarnarà els Estat Units i l'altre la Unió Soviètica. Qui dominarà el món?\", 5599, 1, \"/twilight-struggle.php\")";
    $this->query($query);
    $query = "INSERT INTO Game (name, description, price, category, url) VALUES (\"Patchwork\", \"En aquest senzill i molt interessant joc, dos jugador competiràn per teixir la tela amb major puntuació.\", 1799, 1, \"/patchwork.php\")";
    $this->query($query);
    $query = "INSERT INTO Game (name, description, price, category, url) VALUES (\"Primer frutal\", \"Un corb es vol menjar tota la fruita abans que arribi al cistell... Ho aconseguirà?\", 2299, 2, \"/primer-frutal.php\")";
    $this->query($query,);
    $query = "INSERT INTO Game (name, description, price, category, url) VALUES (\"Pincha el pirata\", \"Haurem de col·locar les espases al barril on està el pirata, el primer que el fagi saltar guanya.\", 999, 2, \"/pincha-el-pirata.php\")";
    $this->query($query,);
    $query = "INSERT INTO Game (name, description, price, category, url) VALUES (\"Carcassonne Junior\", \"Construeix un mapa diferent cada partida, què consisteix en col·locar fitxes al taulell.\", 2599, 2, \"/carcassone-junior.php\")";
    $this->query($query);
    $query = "INSERT INTO Game (name, description, price, category, url) VALUES (\"Runebound\", \"Un joc d'aventures en qual haurem d'equipar el nostre personatje, fer missions i matar monstres per guanyar la partida.\", 3999, 3, \"/runebound.php\")";
    $this->query($query);
    $query = "INSERT INTO Game (name, description, price, category, url) VALUES (\"Clank!\", \"Serem uns lladres que s'endinsen a una masmorra per robar els tresors... Sortirem amb vida?\", 4999, 3, \"/clank.php\")";
    $this->query($query);
    $query = "INSERT INTO Game (name, description, price, category, url) VALUES (\"Battlestar Galactica\", \"Ambientat en la famosa sèrie, un dels jugador serà Cylon en secret... Aconseguiràn els humans sobreviure l'atac Cylon?\", 5999, 3, \"/battlestar-galactica.php\")";
    $this->query($query);

    // OrderData table
    $this->logging("3.1 Dropping old 'OrderData' table...");
    $query = "DROP TABLE IF EXISTS OrderData;";
    $this->query($query);
    $this->logging("3.2 Creating new 'OrderData' table...");
    $query = "CREATE TABLE OrderData ("
      . "id INT UNSIGNED AUTO_INCREMENT,"
      . "timestamp TIMESTAMP NOT NULL,"
      . "full_name VARCHAR(255) NOT NULL,"
      . "province VARCHAR(255) NOT NULL,"
      . "email VARCHAR(255) NOT NULL,"
      . "address VARCHAR(255) NOT NULL,"
      . "phone VARCHAR(255) NOT NULL,"
      . "price_total INT UNSIGNED NOT NULL,"
      . "PRIMARY KEY(id)"
      . ") ENGINE=INNODB CHARACTER SET utf8 COLLATE utf8_general_ci";
    $this->query($query);

    // OrderLine table
    $this->logging("4.1 Dropping old 'OrderLine' table...");
    $query = "DROP TABLE IF EXISTS OrderLine";
    $this->query($query);
    $this->logging("4.2 Creating new 'OrderLine' table...");
    $query = "CREATE TABLE OrderLine ("
      . "id INT UNSIGNED AUTO_INCREMENT,"
      . "game INT UNSIGNED,"
      . "quantity SMALLINT UNSIGNED NOT NULL,"
      . "price_total INT UNSIGNED NOT NULL,"
      . "order_id INT UNSIGNED,"
      . "PRIMARY KEY(id),"
      . "FOREIGN KEY(game) REFERENCES Game(id),"
      . "FOREIGN KEY(order_id) REFERENCES OrderData(id)"
      . ") ENGINE=INNODB CHARACTER SET utf8 COLLATE utf8_general_ci";
    $this->query($query);

    // Finish
    $this->closeConnection();
  }

  public function query(string $query, $showResult = TRUE)
  {
    $result = $this->database->query($query);
    
    if ($result === FALSE) {
      if($showResult) {
        $this->logging(" KO\n");
        $this->logging($query, TRUE);
      }
      $this->error($this->database->error);
    }
    else {
      if($showResult) {
        $this->logging(" OK\n");
        $this->logging($query, TRUE);
      }
    }
    
    return $result;
  }

  private function logging(string $message, $isCode = FALSE)
  {
    if ($isCode) {
      echo "<pre>";
      print_r($message);
      echo nl2br("</pre>" . "\n");
    }
    else {
      echo nl2br($message);
    }
  }

  private function error($message)
  {
    echo "<pre>";
    if (is_array($message)) {
      print_r($message);
    }
    else {
      print($message);
    }
    echo "</pre>";
    die;
  }

  private function setConnection(array $credentials = self::CREDENTIALS)
  {
    if ($this->isLogger) { 
      $this->logging("-> Connecting to database as '" . $credentials['user'] . "'...", "");
    }
    $this->database = new mysqli($credentials['host'], $credentials['user'], $credentials['pswd'], $credentials['name'], $credentials['port']);
    if ($this->database->connect_error) {
      $this->logging(" KO");
      $this->error($this->database->connect_error);
      die;
    }
    else {
      if ($this->isLogger) {
        $this->logging(" OK\n\n", "");
      }
    }
  }

  public function closeConnection()
  {
    $this->logging("-> Database operations finished, connection closed.", "");
    $this->database->close();
  }

}
