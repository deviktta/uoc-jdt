<?php

require_once __DIR__ . '/../../src/Entity/Game.php';
require_once __DIR__ . '/SystemService.php';

/**
 * Servei per donar a tasques pròpies del sistema.
 */
class CartService {
  
  protected $order_lines;
  
  public function __construct() {
    // Recuperem la cistella.
    $cart = ($_SESSION['cart']) ?? [];
    
    $order_lines = [];
    foreach ($cart as $game_id => $game_quantity) {
      $game = new Game($game_id);

      $price_unit = (float) $game->get('price') / 100;
      $price_unit_str = number_format($price_unit, 2, ",", ".");
      $price_total = $price_unit * $game_quantity;
      $price_total_str = number_format($price_total, 2, ",", ".");

      $order_line = [
        'id' => $game->get('id'),
        'name' => $game->get('name'),
        'quantity' => $game_quantity,
        'price_total' => $price_total,
        'price_unit_str' => $price_unit_str,
        'price_total_str' => $price_total_str,
      ];
      $order_lines[] = $order_line;
    }
    
    $this->order_lines = $order_lines;
  }
  
  public function destroy() {
    unset($_SESSION['cart']);
  }
  
  public function getOrderLines() {
    return $this->order_lines;
  }
  
  public function getOrderPrice() {
    $total = 0;
    
    foreach ($this->order_lines as $order_line) {
      $total += $order_line['price_total'];
    }

    return $total;
  }
  
  public function printOrderPrice() {
    return number_format($this->getOrderPrice(), 2, ",", ".");;
  }
  
  public static function update(string $game_id, int $quantity) {
    // Recuperem la cistella.
    $cart = ($_SESSION['cart']) ?? [];
    
    // Actualitzem el producte a la cistella
    if (!empty($cart[$game_id])) {
      $cart[$game_id] += $quantity;
    }
    else {
      $cart[$game_id] = $quantity;
    }
    
    if ($cart[$game_id] <= 0) {
      unset($cart[$game_id]);
    }

    // Actualitzem la cistella.
    $_SESSION['cart'] = $cart;
  }
  
  public static function addGameToCart(string $game_id, int $quantity) {
    // Recuperem la cistella.
    $cart = ($_SESSION['cart']) ?? [];
    
    // Actualitzem el producte a la cistella
    if (!empty($cart[$game_id])) {
      $cart[$game_id] += $quantity;
    }
    else {
      $cart[$game_id] = $quantity;
    }

    // Actualitzem la cistella.
    $_SESSION['cart'] = $cart;
  }
  
  public static function getCurrentQuantity() {
    $quantity = 0;
    
    // Recuperem la cistella.
    $cart = ($_SESSION['cart']) ?? [];
    
    // Sumem tots els productes
    foreach ($cart as $game_id => $game_quantity) {
      $quantity += $game_quantity;
    }

    return $quantity;
  }

}