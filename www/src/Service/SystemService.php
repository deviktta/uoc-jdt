<?php

// Habilitar la visualització d'errors PHP.
ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);

// Iniciar sessió de PHP.
session_start();

/**
 * Servei per donar a tasques pròpies del sistema.
 */
class SystemService {
  
  /**
   * Idioma actual.
   *
   * @var string
   */
  protected $current_lang;

  public function getCurrentLang() {
    return $this->currentLang;
  }

  public function setCurrentLang(string $current_lang) {
    $this->currentLang = $current_lang;
  }
  
  public static function debug($data, $isEnd = TRUE) {
    echo "<pre>";
    print_r($data);
    echo "</pre>";

    if ($isEnd) {
      die;
    }
  }
  
  public static function setAdmin(bool $isAdmin) {
    $_SESSION['isAdmin'] = $isAdmin;
  }
  
  public static function isAdmin() : bool {
    return ($_SESSION['isAdmin']) ?? FALSE;
  }

}