# Work for Universitat Oberta de Catalunya

## About the project
**License:**
* This project is under the [GPL v3](https://www.gnu.org/licenses/gpl-3.0.txt) license.

**About the project owner:**
* E-mail: deviktta@protonmail.com
* Secure contact: https://keys.openpgp.org/search?q=deviktta%40protonmail.com
* Made with love from Barcelona, Catalunya.